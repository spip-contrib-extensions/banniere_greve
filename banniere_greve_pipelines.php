<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function banniere_greve_insert_head_css($flux) {
	$flux .= '<link rel="stylesheet" href="'.find_in_path('banniere_greve.css').'" />';
	return $flux;
}

function banniere_greve_affichage_final($page) {
	// ne rien faire si la page en cours n'est pas du html
	if (!$GLOBALS['html']) {
		return $page;
	}
	include_spip('inc/config');
    if (!(lire_config('banniere_greve/desactiver_banniere')=='on')){
        $badge = recuperer_fond('modeles/banniere_greve', []);
        // Insertion du badge avant la fermeture du body
        if (!strpos($page, 'id="banniere_greve"')) {
            $page = preg_replace(',<body[^>]*>,i', '\0'."\n$badge", $page, 1);
        }
    }
	return $page;
}
