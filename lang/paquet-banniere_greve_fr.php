<?php


if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'banniere_greve_description' => 'Ce plugin permet d\'afficher une bannière dans les pages publiques de votre site pour inciter les visiteurs à faire grève.
_ Image de la bannière : CC-BY Les petits débrouillards Bretagne.',
	'banniere_greve_slogan' => 'Une bannière pour appeler à la grève',
);
