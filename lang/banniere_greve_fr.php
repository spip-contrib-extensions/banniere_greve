<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'explication_class_don' => 'Classe(s) CSS à utiliser (exemple : "laclasse" ou "classe1 classe2").',
	'label_class_don' => 'Classe CSS',
	'explication_url_greve' => 'URL ou raccourci de type SPIP (art23 ou rub32 par exemple).',
	'label_url_don' => 'Adresse de la page de dons',
	'titre_banniere_greve' => 'Bannière de grève',
	'titre_lien' => 'La grève, c\'est un acquis qui protège'
	
);
